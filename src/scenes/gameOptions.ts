// CONFIGURABLE GAME OPTIONS

export const GameOptions = {

    // first platform position, in height ratio. In this case, in the top 2/10 of the screen
    firstPlatformPosition: 2 / 10,
    
    // Arcade physics gravity
    gameGravity: 1700,

    // platform horizontal speed range, in pixels per second
    platformHorizontalSpeedRange: [250, 400],

    // platform width range, in pixels
    platformWidthRange: [120, 300],

    // platform vertical distance range, in pixels
    platformVerticalDistanceRange: [150, 250],

    // platform height, in pixels
    platformHeight: 50
}