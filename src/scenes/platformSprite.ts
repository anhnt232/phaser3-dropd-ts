// PLATFORM SPRITE CLASS    

// modules to import
import { GameOptions } from './gameOptions';

// platform sprite extends RenderTexture class
export default class PlatformSprite extends Phaser.GameObjects.RenderTexture {

    // does this platform have the player on it?
    isHeroOnIt: Boolean = false;

    // platform physics body
    body: Phaser.Physics.Arcade.Body;

    // platform assigned velocity
    assignedVelocity: number = 0;

    // can the player land on this platform?
    canLandOnIt: Boolean = false;

	// constructor
    constructor(scene: Phaser.Scene, x: number, y: number, width: number, height: number) {
		super(scene, x, y, width, height);
        
        // set platform origin in horizontal and vertical middle
        this.setOrigin(0.5);

        // add existing platform to scene
        scene.add.existing(this);

        // add a physics body to platform
        scene.physics.add.existing(this);
    }

    // method to set physics properties
    setPhysics(): void {

        // set the body immovable, it won't react to collisions 
        this.body.setImmovable(true);

        // do not allow gravity to move the body
        this.body.setAllowGravity(false);

        // set body friction to max (1), or player sprite will slip away
        this.body.setFrictionX(1);
    }

    // method to draw platform texture
    drawTexture(border: Phaser.GameObjects.Graphics, pattern: Phaser.GameObjects.TileSprite, eyes: Phaser.GameObjects.Sprite): void {
        
        // clear border graphics
        border.clear();

        // set border line style
        border.lineStyle(8, 0x000000, 1);

        // stroke a rectangle
        border.strokeRect(0, 0, this.displayWidth, this.displayHeight);

        // draw pattern texture on platform
        this.draw(pattern, this.displayWidth / 2, Phaser.Math.Between(0, GameOptions.platformHeight));

        // draw eyes texture on platform
        this.draw(eyes, this.displayWidth / 2, this.displayHeight / 2);

        // draw border graphics on platform
        this.draw(border);
    }

    // method to resize the platform
    transformTo(x: number, y: number, width: number, height: number): void {

        // change platform x position
        this.x = x;

        // change platform y position
        this.y = y;

        // set new platform size
        this.setSize(width, height);

        // set new Arcade body size
        this.body.setSize(width, height);
    }

    // method to explode and destroy the platform    
    explodeAndDestroy(emitter: Phaser.GameObjects.Particles.ParticleEmitter): void {

        // get platform bounds
        let platformBounds: Phaser.Geom.Rectangle = this.getBounds();

        // place emitter in top left vertex of the platform
        emitter.setPosition(platformBounds.left, platformBounds.top);

        // set the emitter as active
        emitter.active = true;

        // set emit zone to cover the entire platform
        emitter.setEmitZone({
            source: new Phaser.Geom.Rectangle(0, 0, platformBounds.width, platformBounds.height),
            type: 'random',
            quantity: 50
        });

        // make emitter explode
        emitter.explode(50, this.x - this.displayWidth / 2, this.y - this.displayHeight / 2);

        // remove platform tint
        this.clearTint();

        // player is no longer on this platform
        this.isHeroOnIt = false;

        // player can't land on this platform
        this.canLandOnIt = false;
    }
}
