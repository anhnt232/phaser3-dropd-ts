import 'phaser';

import CONFIG from "../../config";

export default class GameMenu extends Phaser.Scene {
    playButton: Phaser.GameObjects.Sprite;
    logo: Phaser.GameObjects.Sprite;
    scoreText: Phaser.GameObjects.BitmapText;
    bestScoreText: Phaser.GameObjects.BitmapText;
    bestScore: number;
    
    constructor() {
        super("GameMenu");
    }
    preload(){
        // this is how we preload an image
        this.load.image('hero', 'assets/sprites/hero.png');
        this.load.image('pattern', 'assets/sprites/pattern.png');
        this.load.image('eyes', 'assets/sprites/eyes.png');
        this.load.image('particle', 'assets/sprites/particle.png');
        this.load.image('sky', 'assets/sprites/background.png');

		this.load.image("logo", "assets/sprites/Banner_hifpt.png");
        this.load.image("play", "assets/sprites/play.png");
		this.load.image("gameOver", "assets/sprites/GameOver.png");
        this.load.image("scorepanel", "assets/sprites/scorepanel.png");
        this.load.image("scorelabels", "assets/sprites/scorelabels.png");
        this.load.bitmapFont("font", "assets/fonts/font.png", "assets/fonts/font.fnt");
    }
    create() {

        var text = this.add.text(this.scale.width/2 , this.scale.height/4, "Welcome to", {
            font: "bold 60px Arial",
            color: "#6e43d9"
        });
        text.setOrigin(0.5, 1);
        var text = this.add.text(this.scale.width/2 , this.scale.height/4+100, "DROP Game", {
            font: "bold 70px Arial",
            color: "#6e43d9"
        });
        text.setOrigin(0.5, 1);
        this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2, "play");
        this.playButton.setScale(1)
        this.playButton.setOrigin(0.5,0.5);
        this.playButton.setInteractive();
        this.playButton.on("pointerdown", function(){
            
            this.scene.start("GamePlay");
        }, this)
        

        this.bestScore = localStorage.getItem(CONFIG.localStorageBestScoreName) == null ? 0 : Number(localStorage.getItem(CONFIG.localStorageBestScoreName));

        let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", "0", 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -10, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1);
    }
    
}
