import 'phaser';
import CONFIG from "../../config";

export default class GameOver extends Phaser.Scene {
    logo: Phaser.GameObjects.Sprite;
    bestScore: number;
    scoreText: Phaser.GameObjects.BitmapText;
    bestScoreText: Phaser.GameObjects.BitmapText;
    playButton: Phaser.GameObjects.Sprite;
    score: number;
    
    constructor() {
        super("GameOver");
    }

    create() {

        this.score = CONFIG.currentScore;

        this.bestScore = localStorage.getItem(CONFIG.localStorageName) == null ? 0 : Number(localStorage.getItem(CONFIG.localStorageName));

        let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", this.score.toString(), 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);

        var text = this.add.text(this.scale.width/2 , this.scale.height/4+50, "DROP", {
            font: "bold 70px Arial",
            color: "#6e43d9"
        });
        text.setOrigin(0.5, 1);

        var gameOver = this.add.sprite(this.scale.width/2, this.scale.height/4+150, "gameOver");
        gameOver.setScale(1)
        gameOver.setOrigin(0.5,1);

        this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2, "play");
        this.playButton.setScale(1)
        this.playButton.setOrigin(0.5,0.5);
        this.playButton.setInteractive();
        this.playButton.on("pointerdown", function(){
            
            this.scene.start("GamePlay");
        }, this)

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -10, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1);

    }
}
