// MAIN GAME FILE

// modules to import
import 'phaser';
import GamePlay from './scenes/GamePlay';
import GameMenu from './scenes/GameMenu';
import GameLevelUp from "./scenes/GameLevelUp";
import GameOver from "./scenes/GameOver";
import { GameOptions } from './scenes/gameOptions';

let game : Phaser.Game;

window.onload = function() {

    let gameConfig = {
        type: Phaser.AUTO,
        backgroundColor: 0xB6D3FF,
        scale: {
            mode: Phaser.Scale.FIT,
            autoCenter: Phaser.Scale.CENTER_BOTH,
            parent: "thegame",
            width: 800,
            height: 800*16/9
        },
        physics: {
            default: "arcade",
            arcade: {
                gravity: {
                    y: GameOptions.gameGravity
                }
            }
        },
        scene: [GameMenu, GamePlay, GameLevelUp, GameOver],

    }
    game = new Phaser.Game(gameConfig);
    window.focus();
    resize();
    window.addEventListener("resize", resize, false);

}

function resize(){
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let windowRatio = windowWidth / windowHeight;
    let gameRatio = Number(game.config.width) / Number(game.config.height);
    if(windowRatio < gameRatio){
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else{
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}

// // object to initialize the Scale Manager
// const scaleObject: Phaser.Types.Core.ScaleConfig = {
//     mode: Phaser.Scale.FIT,
//     autoCenter: Phaser.Scale.CENTER_BOTH,
//     parent: 'thegame',
//     width: 750,
//     height: 1334
// }

// // obhect to initialize Arcade physics
// const physicsObject: Phaser.Types.Core.PhysicsConfig = {
//     default: 'arcade',
//     arcade: {
//         gravity: {
//             y: GameOptions.gameGravity
//         }
//     }    
// }

// // game configuration object
// const configObject: Phaser.Types.Core.GameConfig = {
//     type: Phaser.AUTO,
//     backgroundColor:0x87ceea,
//     scale: scaleObject,
//     scene: [GameMenu, GamePlay],
//     physics: physicsObject
// }

// // the game itself
// new Phaser.Game(configObject);


