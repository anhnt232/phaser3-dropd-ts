// THE GAME ITSELF

// modules to import
import { GameOptions } from './gameOptions';
import PlayerSprite from './playerSprite';
import PlatformSprite from './platformSprite';

// this class extends Scene class
export class PlayGame extends Phaser.Scene {

    // sprite with platform eyes
    eyes: Phaser.GameObjects.Sprite;

    // graphics where to draw platform borders
    borderGraphics: Phaser.GameObjects.Graphics;

    // tile sprite, used for platform background
    spritePattern: Phaser.GameObjects.TileSprite;

    // the hero!
    hero: PlayerSprite;
  
    // explosion emitter
    emitter: Phaser.GameObjects.Particles.ParticleEmitter;

    // particles to be emitted
    particles:  Phaser.GameObjects.Particles.ParticleEmitterManager;

    // group which will contain all platforms
    platformGroup: Phaser.Physics.Arcade.Group;

    // here we store game width once for all
    gameWidth: number;

    // here we store game height once for all
    gameHeight: number;

    // camera following game action
    actionCamera: Phaser.Cameras.Scene2D.Camera;

    // background sky
    sky: Phaser.GameObjects.Sprite;
   
    // constructor
    constructor() {
        super({
            key: 'PlayGame'
        });
    }

    // method to be called once the class has been created
    create(): void {

        // save game width value
        this.gameWidth = this.game.config.width as number;

        // save game height value
        this.gameHeight = this.game.config.height as number;
        
        // method to add the background sky
        this.addSky();

        // add a new Graphics object...
        this.borderGraphics = this.add.graphics();

        // ... and set it to invisible
        this.borderGraphics.setVisible(false);

        // add a new tile sprite...
        this.spritePattern = this.add.tileSprite(this.gameWidth / 2, GameOptions.platformHeight / 2,this.gameWidth, GameOptions.platformHeight * 2, 'pattern');
        
        // ... and set it to invisible
        this.spritePattern.setVisible(false);

        // add a new sprite...
        this.eyes = this.add.sprite(0, 0, 'eyes');

        // ... and set it to invisible
        this.eyes.setVisible(false);
        
        // create a new physics group
        this.platformGroup = this.physics.add.group();
        
        // we are going to add 12 platforms, and reuse them when we need them
        for (let i: number = 0; i < 12; i ++) {

            // method to add a platform
            this.addPlatform(i == 0);
        }

        // add the hero
        this.hero = new PlayerSprite(this, this.gameWidth / 2, 0, 'hero');
	    
        // input listener
        this.input.on('pointerdown', this.destroyPlatform, this);
        
        // method to add the emitter
        this.createEmitter();
     
        // method to set the cameras
        this.setCameras();
    }

    // method to add the sky
    addSky(): void {

        // add sky sprite
        this.sky = this.add.sprite(0, 0, 'sky');

        // adjust sprite size to cover the entire screen
        this.sky.displayWidth = this.gameWidth;
        this.sky.displayHeight = this.gameHeight;

        // set sprite origin to top left corner
        this.sky.setOrigin(0, 0);
    }

    // method to set the cameras
    setCameras(): void {

        // add a new camera
        this.actionCamera = this.cameras.add(0, 0, this.gameWidth, this.gameHeight);

        // action camera won't render the sky
        this.actionCamera.ignore([this.sky]);

        // make action camera follow the hero
        this.actionCamera.startFollow(this.hero, true, 0, 0.5, 0, - (this.gameHeight / 2 - this.gameHeight * GameOptions.firstPlatformPosition));

        // main camera won't render, particles, hero and platforms
        this.cameras.main.ignore([this.particles]);
        this.cameras.main.ignore([this.hero]);
        this.cameras.main.ignore(this.platformGroup);
    }

    // method to crate the emitter
    createEmitter(): void {

        // create new particles
        this.particles = this.add.particles('particle');
        
        // create the emitter
        this.emitter = this.particles.createEmitter({
            
            // particles will scale from 1 (original size) to zero (too tiny to be seen)
            scale: {
                start: 1,
                end: 0
            },

            // speed will scale from zero to 200 pixels per second
            speed: {
                min: 0,
                max: 200
            },

            // emitter is not active at the moment
            active: false,

            // particle lifespan, in milliseconds
            lifespan: 500,

            // quantity of particles to be used
            quantity: 50
        });
    }

    // method to add a platform 
    addPlatform(isFirst: Boolean): void {
        
        // creation of a new platform sprite
        let platform: PlatformSprite = new PlatformSprite(this, this.gameWidth / 2, isFirst ? this.gameWidth * GameOptions.firstPlatformPosition : 0, this.gameWidth / 8, GameOptions.platformHeight);
        
        // add platform to platformGroup group
        this.platformGroup.add(platform);

        // call platform setPhysics method
        platform.setPhysics();

        // call platform drawTexture method
        platform.drawTexture(this.borderGraphics, this.spritePattern, this.eyes);

        // is this the first platform?
        if (isFirst) {

            // paint it green
            platform.setTint(0x00ff00);
        }
        else {

            // initialize the platform
            this.initPlatform(platform);
        }
    }

    // method to destroy a platform
    destroyPlatform(): void {

        // can the hero destroy the platform and is the hero alive?
        if (this.hero.canDestroyPlatform && !this.hero.isDying) {

            // hero can't destroy the platform anymore
            this.hero.canDestroyPlatform = false;

            // get the platform body which is the closest to the hero
            let closestPlatform: Phaser.Physics.Arcade.Body = this.physics.closest(this.hero) as Phaser.Physics.Arcade.Body;

            // given the body, get the sprite
            let platform: PlatformSprite = closestPlatform.gameObject as PlatformSprite;

            // make platform explode
            platform.explodeAndDestroy(this.emitter);

            // initialize the platform again
            this.initPlatform(platform);
        }
    }

    // method to initialize the platform
    initPlatform(platform: PlatformSprite): void {

        // set assignedVelocity property a random value
        platform.assignedVelocity = this.randomValue(GameOptions.platformHorizontalSpeedRange) * Phaser.Math.RND.sign();
        
        // change platform size
        platform.transformTo(this.gameWidth / 2, this.getLowestPlatform() + this.randomValue(GameOptions.platformVerticalDistanceRange), this.randomValue(GameOptions.platformWidthRange), GameOptions.platformHeight);
        
        // draw a new texture on the platform
        platform.drawTexture(this.borderGraphics, this.spritePattern, this.eyes);
    }

    // method to get the lowest platform position
    getLowestPlatform(): number {

        // set lowestplatform to zero
        let lowestPlatform: number = 0;

        // get all platforms
        let platforms: PlatformSprite[] = this.platformGroup.getChildren() as PlatformSprite[];
        
        // iterate through all platforms
        for (let platform of platforms) {

            // lowestplatform is the max value between itself and platform vertical position
            lowestPlatform = Math.max(lowestPlatform, platform.y);    
        }

        // return lowestPlatform value
        return lowestPlatform;
    }

    // method to get the highest platform
    getHighestPlatform(maxHeight: number): PlatformSprite {

        // set highest platform y position to Infinity
        let highestPlatformY = Infinity;

        // get all platforms
        let platforms: PlatformSprite[] = this.platformGroup.getChildren() as PlatformSprite[];

        // set an arbitrary platform as highest platform
        let highestPlatform: PlatformSprite = platforms[0];

        // iterate through all platforms
        for (let platform of platforms) {

            // is platform y position is greater than maxHeight and less than highest platform y?
            if (platform.y > maxHeight && platform.y < highestPlatformY) {

                // ... highestPlatform is now the current platform
                highestPlatform = platform;

                // update highest platform y
                highestPlatformY = platform.y;
            }   
        }

        // return highestPlatform
        return highestPlatform;
    }

    // method to toss a random value between two elements in an array
    randomValue(a: number[]): number {
        return Phaser.Math.Between(a[0], a[1]);
    }
    
    // method to check for collision between two bodies
    handleCollision(body1: Phaser.GameObjects.GameObject, body2: Phaser.GameObjects.GameObject): void {

        // first body is the hero
        let hero: PlayerSprite = body1 as PlayerSprite;

        // second body is the platform
        let platform: PlatformSprite = body2 as PlatformSprite;

        // if the hero is not on the platform...
        if (!platform.isHeroOnIt) {

            // is the hero too close to left platform edges or landing on the left side of an illegal platform?
            if (hero.x < platform.getBounds().left || (!platform.isTinted && hero.x <= platform.x)) {

                // make hero fall down and die to the left
                this.fallAndDie(-1);

                // exit the function
                return; 
            }

            // is the hero too close to right platform edges or landing on the right side of an illegal platform?
            if (hero.x > platform.getBounds().right || (!platform.isTinted && hero.x > platform.x)) {

                 // make hero fall down and die to the right
                this.fallAndDie(1);

                // exit the function
                return; 
            }

            // now this platform has the hero on it
            platform.isHeroOnIt = true;

            // method to paint safe platforms
            this.paintSafePlatforms();

            // hero can destroy platforms once more
            this.hero.canDestroyPlatform = true;
        }
    }

    // method to make the hero fall and die
    fallAndDie(mult: number): void {

        // call hero's die method
        this.hero.die(mult);

        // create a time event
        this.time.addEvent({

            // after 800 milliseconds...
            delay: 800,
            callbackScope: this,
            callback: function() {

                // make action camera to stop following the hero
                this.actionCamera.stopFollow();
            }
        });
    }

    // method to paint safe platforms
    paintSafePlatforms(): void {

        // set floor platform as the highest platform
        let floorPlatform: PlatformSprite = this.getHighestPlatform(0);

        // tint floor platform red
        floorPlatform.setTint(0xff0000);

        // set target platform as the highest platform, but below floor platform
        let targetPlatform: PlatformSprite = this.getHighestPlatform(floorPlatform.y);

        // tint target platform green
        targetPlatform.setTint(0x00ff00);

        // player can land on this platform
        targetPlatform.canLandOnIt = true;
    }

    // method to be called at each frame
    update(): void {

        // is hero alive?
        if (!this.hero.isDying) {

            // manage collisions between the hero and platforms
            this.physics.world.collide(this.hero, this.platformGroup, this.handleCollision, undefined, this);
        }

        // get all platforms
        let platforms: PlatformSprite[] = this.platformGroup.getChildren() as PlatformSprite[];

        // iterate through all platforms
        for (let platform of platforms) {

            // if platform y position plus game height is less than hero y position...
            if (platform.y + this.gameHeight < this.hero.y) {

                // game over, let's start again
                this.scene.start('PlayGame');    
            }   

            // check distance between the platform and game edges
            let distance: number = Math.max(0.2, 1 - ((Math.abs(this.gameWidth / 2 - platform.x) / (this.gameWidth / 2)))) * Math.PI / 2;

            // assign platform velocity
            platform.body.setVelocityX(platform.assignedVelocity * distance);

            // if a platform reached the left or right edge...
            if ((platform.body.velocity.x < 0 && platform.getBounds().left < this.hero.displayWidth / 2) || (platform.body.velocity.x > 0 && platform.getBounds().right > this.gameWidth - this.hero.displayWidth / 2)) {
                
                // ... invert its velocity
                platform.assignedVelocity *= -1;
           }
        }
    }
}