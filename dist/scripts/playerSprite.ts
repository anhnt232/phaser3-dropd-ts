// PLATFORM SPRITE CLASS    

// player sprite extends Sprite class
export default class PlayerSprite extends Phaser.Physics.Arcade.Sprite {

    // can the player destroy the platform?
    canDestroyPlatform: Boolean = false;

    // is the hero dying?
    isDying: Boolean = false;

    // scene which called this class
    mainScene: Phaser.Scene;

    // constructor
	constructor(scene: Phaser.Scene, x: number, y: number, key: string) {
		super(scene, x, y, key);

        // add the player to the scnee
        scene.add.existing(this);

        // add physics body to platform
        scene.physics.add.existing(this);

        // save the scene which called this class
        this.mainScene = scene;
	}

    // method to make the player die
    die(mult: number): void {

        // hero is dying
        this.isDying = true;

        // set vertical velocity to -200 pixels per second (going up)
        this.setVelocityY(-200);

        // set horizontal velocity to 200 or -200 pixels per second
        this.setVelocityX(200 * mult);

        // set player angle to 45 or -45 using a 0.5s tween
        this.mainScene.tweens.add({
            targets: this,
            angle: 45 * mult,
            duration: 500
        });
    }
}
